import variables from './variables';

export const gradientColors = {
	ribbonGradient: {
		gradientColorFrom : '#3e5290',
		gradientColorTo: '#293967',
	},
	collectionTitle: {
		gradientColorFrom: '#3f5692',
		gradientColorTo: '#566fb2',
	},
};

export const mixins = {
	THEME_FONT: {
		fontFamily: variables.FONTS.FONT_THEME,
		color: variables.COLORS.BASIC_FONT,
	},
};

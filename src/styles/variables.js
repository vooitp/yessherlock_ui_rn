export default {
	FONTS: {
		SMALL: 12,
		MEDIUM: 14,
		LARGE: 20,
		HUGE: 28,
		LIGHT: 200,
		WEIGHT_MEDIUM: 600,
		WEIGHT_HEAVY: 800,
		FONT_THEME: 'BerkshireSwash-Regular',
	},
	COLORS: {
		THEME_DARK_BLUE: 'rgb(62, 42, 101)',
		THEME_NAVY_BLUE: 'rgb(39, 35, 51)',
		THEME_LIGHT_BLUE: 'rgb(101, 73, 172)',
		THEME_ULTRA_LIGHT_BLUE: 'rgb(168, 141, 240)',
		THEME_GREEN: 'rgb(82, 72, 68)',
		BASIC_FONT: 'rgb(213, 192, 76)',
		THEME_DARK_GREEN: '#352f2c',
		THEME_BORDER: '#876e3f',
	},
};

import mixins from './mixins'
import variables from './variables'
import { StyleSheet } from "react-native";

export const generalStyles = StyleSheet.create({
	text__blue : {
		color: variables.COLORS.THEME_ULTRA_LIGHT_BLUE,
	},
	col50: {
		width: '50%',
	},
	icon: {
		width: 16,
		height: 16,
		resizeMode: 'contain',
	},
	row: {
		flexDirection: 'row',
	}
});

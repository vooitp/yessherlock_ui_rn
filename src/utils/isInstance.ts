export const isInstance = (obj: any, Class: any) => {
	for (const prop in new Class()) {
		if (typeof obj[prop] === 'undefined') {
			return false;
		}
	}
	return true;
};

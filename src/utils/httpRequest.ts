import axios from 'axios';
import Jsona from 'jsona';
import { Dispatch } from 'redux';
import { addLoadingFlag, removeLoadingFlag } from 'src/redux/loadingFlags/actions';
import { GlobalDataService } from '../services/globalDataService';

const dataFormatter = new Jsona();

const baseUrl = 'http://localhost:9000/api';

export const makeRequest = (
	type: 'get' | 'post' | 'delete',
	path: string,
	data?: any,
	dispatch?: Dispatch,
	actionId?: string,
) => {
	const config = {
		headers: {
			'Content-Type': 'application/vnd.api+json',
			'Authorization': `Bearer ${GlobalDataService.apiToken}`,
		},
	};

	const url = baseUrl + path;
	let request = null;
	switch (type) {
		case 'get': {
			request = axios.get(url, config);
			break;
		}
		case 'post': {
			request = axios.post(url, { data: { attributes: data }}, config);
			break;
		}
		case 'delete': {
			request = axios.delete(url, config);
			break;
		}
	}

	if (request) {
		if (dispatch && actionId) {
			dispatch(addLoadingFlag(actionId));
		}
		return request
			.then(response => {
				if (dispatch && actionId) {
					dispatch(removeLoadingFlag(actionId));
				}
				return [null, dataFormatter.deserialize(response.data)];
			})
			.catch(err => {
				if (dispatch && actionId) {
					dispatch(removeLoadingFlag(actionId));
				}
				return [dataFormatter.deserialize(err), null];
			});
	}

	throw new Error('UNKNOW REQUEST TYPE');
};

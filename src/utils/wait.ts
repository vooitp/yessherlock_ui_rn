export let wait = (promise: Promise<any>): Promise<{data: any, err: any}> => {
	return (promise
		.then(data => {
			if (data) {
				return { data };
			}
			return { err: data };
		})
		.catch(err => ({ err })) as Promise<{data: any, err: any}>);
};

import { AccessToken } from 'react-native-fbsdk';
import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

export const FacebookService = {
	readPermissions: ['public_profile', 'email'],

	getAccessToken() {
		return AccessToken.getCurrentAccessToken().then(data => {
			if (data) {
				return data.accessToken.toString();
			}
			return null;
		});
	},

	getUserData(accessToken: string, fields: string[] = []) {
		return new Promise((resolve, reject) => {
			const infoRequest = new GraphRequest(
				`/me?fields=${fields.join(',')}`,
				{ accessToken },
				(error: object | undefined, result: object | undefined) => {
					if (result) {
						resolve(result);
					} else {
						reject(error);
					}
				},
			);
			new GraphRequestManager().addRequest(infoRequest).start();
		});
	},
};

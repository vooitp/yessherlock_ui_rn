import { makeRequest } from 'src/utils/httpRequest';
import { Dispatch } from 'redux';
import { MatchModel } from '../interfaces/match';
import { GET_DATA_DONE, POST_DATA } from '../redux/matches/constants';

export const MathesService = {
	async fetchMatchesData(dispatch: Dispatch): Promise<MatchModel> {
		const [err, data] = await makeRequest(
			'get',
			`/matches?include=card,host&onlyPrivate=true`,
			null,
			dispatch,
			GET_DATA_DONE,
		);

		if (data) {
			return Promise.resolve(data as MatchModel);
		}
		return Promise.reject(err);
	},
	async postNewMatchData(dispatch: Dispatch, payload: any): Promise<any> {
		const [err, data] = await makeRequest(
			'post',
			'/matches',
			payload,
			dispatch,
			POST_DATA,
		);
		if (data) {
			return Promise.resolve(data);
		}
	},

};

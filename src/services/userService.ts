import { makeRequest } from 'src/utils/httpRequest';
import { SET_USER_DATA } from 'src/redux/user/constants';
import { Dispatch } from 'redux';
import { isInstance } from 'src/utils/isInstance';
import { User } from 'src/interfaces/user';

export const UserService = {
	async fetchUserData(dispatch: Dispatch): Promise<User | any> {
		const [err, data] = await makeRequest(
			'get',
			'/user',
			null,
			dispatch,
			SET_USER_DATA,
		);

		if (isInstance(data, User)) {
			return Promise.resolve(data as User);
		}

		return Promise.reject(err);
	},

	async loginUser(facebookId: string, name: string, email: string, picture: string): Promise<string | any> {
		const [err, data] = await makeRequest(
			'post',
			'/user/login',
			{ facebookId, name, email, picture },
		);

		if (data && data.token) {
			return Promise.resolve(data.token);
		}

		return Promise.reject(err);
	},
};

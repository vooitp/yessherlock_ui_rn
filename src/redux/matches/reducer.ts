import {
	GET_DATA_REQUESTED,
	GET_DATA_DONE,
	GET_DATA_FAILED,
	CREATE_MATCH_SUCCESS,
	CREATE_MATCH_FAILURE,
} from './constants';

export const matchesReducer = (state: any = [], action: { type: any; payload?: any; }) => {
	switch (action.type) {
		// case GET_DATA_REQUESTED:
		// 	return { ...state };
		case GET_DATA_DONE:
			return action.payload;
		case GET_DATA_FAILED:
			return { ...state };
		default:
			return state;
	}
};

export const newMatchReducer = (state: any = [], action) => {
	switch (action.type) {
		case CREATE_MATCH_SUCCESS:
			return action.payload;
		case CREATE_MATCH_FAILURE:
			return { ...state };
		default:
			return state;
	}
};

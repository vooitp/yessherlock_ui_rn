import {
	GET_DATA_REQUESTED,
	GET_DATA_DONE,
	GET_DATA_FAILED,
	POST_DATA,
	CREATE_MATCH_SUCCESS,
	CREATE_MATCH_FAILURE,
} from './constants';

import { wait } from '../../utils/wait';
import { MathesService } from '../../services/matchService';
import { MatchModel } from '../../interfaces/match';
import { Dispatch } from 'redux';

// export function getDataRequested() {
// 	return {
// 		type: GET_DATA_REQUESTED,
// 	};
// }

export function getDataDone(data: MatchModel[]) {
	return {
		type: GET_DATA_DONE,
		payload: data,
	};
}

export function getDataFailed(error: any) {
	return {
		type: GET_DATA_FAILED,
		payload: error,
	};
}

export const getData = (dispatch: Dispatch) => {
	return async (): Promise<any> => {
		const {err, data} = await wait(MathesService.fetchMatchesData(dispatch));

		if (data) {
			dispatch(getDataDone(data));
			return Promise.resolve(data);
		} else {
			dispatch(getDataFailed(err));
			return Promise.reject(err);
		}
	};
};

// ----------------------------

const createMatchSuccess = (data: any) => ({
	type: CREATE_MATCH_SUCCESS,
	payload: {
		...data,
	},
});
const addTodoFailure = (error: any) => ({
	type: CREATE_MATCH_FAILURE,
	payload: {
		error,
	},
});

export const postData = (dispatch: Dispatch) => {
	return async (payload): Promise<any> => {
		const {err, data} = await wait(MathesService.postNewMatchData(dispatch, payload));
		if (data) {
			dispatch(createMatchSuccess(data));
			return Promise.resolve(data);
		}
		dispatch(addTodoFailure(err));
		return Promise.reject(err);
	};
};

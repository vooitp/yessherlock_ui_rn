export const createLoadingFlagSelector = (actionId: string) => (state: string[]) => {
	return state.indexOf(actionId);
};

import { ADD_LOADING_FLAG, REMOVE_LOADING_FLAG } from './constants';

export const addLoadingFlag = (id: string) => {
	return {
		type: ADD_LOADING_FLAG,
		id,
	};
};

export const removeLoadingFlag = (id: string) => {
	return {
		type: REMOVE_LOADING_FLAG,
		id,
	};
};

import { ADD_LOADING_FLAG, REMOVE_LOADING_FLAG } from './constants';

interface Action {
	type: string;
	id: string;
}

export const loadingFlagReducer = (state = [] as string[], action: Action) => {
	switch (action.type) {
		case ADD_LOADING_FLAG:
			if (state.indexOf(action.id) === -1) {
				return [
					...state,
					action.id,
				];
			}
			return state;
		case REMOVE_LOADING_FLAG:
			return state.filter(loadingFlagId => loadingFlagId !== action.id);
		default:
			return state;
	}
};

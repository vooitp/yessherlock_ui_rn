import {
	INITIALIZE_LIST,
	UPDATE_LIST,
	DELETE_LIST,
	REQUEST_UPDATE_LIST,
	APPEND_TO_LIST,
	REQUEST_APPEND_TO_LIST,
} from './constants';

interface Action {
	type: string;
	name: string;
}

export const dataListsReducer = (state: any = {}, action: Action) => {
	switch (action.type) {
		case INITIALIZE_LIST:
			return {
				...state,
				[action.name]: [],
			};
		// this action should etrevie corresponding form values && HTTP req with them
		// case REQUEST_UPDATE_LIST:
		// 	return {
		// 		...state,
		// 		 [action.name]: [],
		// 	};
		case UPDATE_LIST:
			return {
				...state,
				[action.name]: {
					...state[action.name],
					...action.formValue,
				},
			};

		case DELETE_LIST: {
			const { [action.name]: targetForm, ...rest } = state;
			return rest;
		}
		default:
			return state;
	}
};

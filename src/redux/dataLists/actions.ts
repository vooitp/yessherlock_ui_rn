import {
	INITIALIZE_LIST,
	UPDATE_LIST,
	DELETE_LIST,
	REQUEST_UPDATE_LIST,
	APPEND_TO_LIST,
	REQUEST_APPEND_TO_LIST,
} from './constants';

export const initializeList = (name: string) => {
	return {
		type: INITIALIZE_LIST,
		name,
	};
};

export const requestUpdateList = (name: string) => {
	return {
		type: REQUEST_UPDATE_LIST,
		name,
	};
};

export const updateList = (name: string) => {
	return {
		type: UPDATE_LIST,
		name,
	};
};

export const requestAppendToList = (name: string) => {
	return {
		type: REQUEST_APPEND_TO_LIST,
		name,
	};
};

export const appendToList = (name: string) => {
	return {
		type: APPEND_TO_LIST,
		name,
	};
};

export const deleteList = (name: string) => {
	return {
		type: DELETE_LIST,
		name,
	};
};

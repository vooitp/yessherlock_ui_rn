export const INITIALIZE_LIST = 'loading/INITIALIZE_LIST';
export const UPDATE_LIST = 'loading/UPDATE_LIST';
export const REQUEST_UPDATE_LIST = 'loading/REQUEST_UPDATE_LIST';
export const APPEND_TO_LIST = 'loading/APPEND_TO_LIST';
export const REQUEST_APPEND_TO_LIST = 'loading/REQUEST_APPEND_TO_LIST';
export const DELETE_LIST = 'loading/DELETE_LIST';

import { TOGGLE_USER_RIBBON } from './constants';

const initialState = {
	val: true,
};

export const switchReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case 'TOGGLE_USER_RIBBON': {
			return { ...state, val: !state.val };
		}
	}
	return state;
};

import { TOGGLE_USER_RIBBON } from './constants';

export const toggle = () => ({
	type: 'TOGGLE_USER_RIBBON',
});

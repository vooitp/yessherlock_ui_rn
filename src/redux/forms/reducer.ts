import { INITIALIZE_FORM, UPDATE_FORM, DELETE_FORM } from './constants';

interface Action {
	type: string;
	name: string;
	formValue: any;
}

export const formsReducer = (state: any = {}, action: Action) => {
	switch (action.type) {
		case INITIALIZE_FORM:
			return {
				...state,
				[action.name]: action.formValue,
			};

		case UPDATE_FORM:
			// console.log(state)
			return {
				...state,
				[action.name]: {
					...state[action.name],
					...action.formValue,
				},
			};

		case DELETE_FORM: {
			const { [action.name]: targetForm, ...rest } = state;
			return rest;
		}
		default:
			return state;
	}
};

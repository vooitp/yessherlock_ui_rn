import { INITIALIZE_FORM, UPDATE_FORM, DELETE_FORM} from './constants';

export const initializeForm = (name: string, formValue: any = {}) => {
	return {
		type: INITIALIZE_FORM,
		name,
		formValue,
	};
};

export const updateForm = (name: string, formValue: any) => {
	return {
		type: UPDATE_FORM,
		name,
		formValue,
	};
};

export const deleteForm = (name: string) => {
	return {
		type: DELETE_FORM,
		name,
	};
};

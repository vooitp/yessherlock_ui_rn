import { Dispatch } from 'redux';
import { User } from 'src/interfaces/user';
import { SET_USER_DATA } from './constants';
import { GlobalDataService } from 'src/services/globalDataService';
import { UserService } from 'src/services/userService';
import { wait } from 'src/utils/wait';

const setUserDataAction = (user: User) => ({
	type: SET_USER_DATA,
	user,
});

const loginUser = () => {
	return async (facebookId: string, name: string, email: string, picture: string): Promise<boolean> => {
		const {err, data: token} = await wait(UserService.loginUser(facebookId, name, email, picture));

		if (token) {
			GlobalDataService.apiToken = token;
			return Promise.resolve(true);
		}

		return Promise.reject(err);
	};
};

const fetchUserData = (dispatch: Dispatch) => {
	return async (): Promise<any> => {
		const {err, data: user} = await wait(UserService.fetchUserData(dispatch));

		if (user) {
			dispatch(setUserDataAction(user));
			return Promise.resolve(user);
		}

		return Promise.reject(err);
	};
};

export const UserActions = {
	loginUser,
	fetchUserData,
};

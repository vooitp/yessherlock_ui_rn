import { SET_USER_DATA } from './constants';

interface Action {
	type: string;
	[key: string]: any;
}

export const userReducer = (state = {}, action: Action) => {
	switch (action.type) {
		case SET_USER_DATA:
			state = action.user;
			// console.log(state)
			return state;
		default:
			return state;
	}
};

import { combineReducers } from 'redux';
import { userReducer } from './user/reducer';
import { switchReducer } from './userRibbon/reducers';
import { formsReducer } from './forms/reducer';
import { dataListsReducer } from './dataLists/reducer';
import { newMatchReducer, matchesReducer } from './matches/reducer';

export default combineReducers({
	user: userReducer,
	value: switchReducer,
	form: formsReducer,
	list: dataListsReducer,
	matches: matchesReducer,
	newMatch: newMatchReducer,
});

import React, { Component } from 'react';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { HomeComponent } from 'src/components/Home';
import { User } from 'src/interfaces/user';
import { UserActions } from 'src/redux/user/actions';

interface Props {
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
	user: User | null;

	loginUser(facebookId: string, name: string, email: string, picture: string): Promise<boolean>;
	fetchUserData(): Promise<User | any>;
}

class Home extends Component<Props> {
	render() {
		const { user, loginUser, navigation, fetchUserData } = this.props;
		return <HomeComponent user={user} loginUser={loginUser} navigation={navigation} fetchUserData={fetchUserData} />;
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
	loginUser: UserActions.loginUser(),
	fetchUserData: UserActions.fetchUserData(dispatch),
});

export const HomePage = connect(
	mapStateToProps,
	mapDispatchToProps,
)(Home);

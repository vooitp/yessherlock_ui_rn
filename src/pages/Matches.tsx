import React, { Component } from 'react';
import { Button, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { MatchesComponent } from 'src/components/Matches';
import { User } from 'src/interfaces/user';
import { getData } from '../redux/matches/actions';

interface Props {
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
	user: User;
	matches: any[];
	getData: any;
}

class Matches extends Component<Props> {

	async componentDidMount(): Promise<void> {
		await this.props.getData();
	}

	render() {
		const { navigation, user, matches } = this.props;
		return (
			<View style={{flexGrow: 1}}>
				<MatchesComponent user={ user } matchesList={ matches }/>
				<Button onPress={() => navigation.navigate('NewMatch')} title='Create match' />
			</View>
		);
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user,
	matches: state.matches,
});

const mapDispatchToProps = (dispatch) => {
	return {
		getData: getData(dispatch),

	};
};

export const MatchesPage = connect(
	mapStateToProps, mapDispatchToProps,
)(Matches);

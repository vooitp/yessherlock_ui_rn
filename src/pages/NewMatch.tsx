import React, { Component } from 'react';
import { View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { connect } from 'react-redux';
import { NewMatchComponent } from 'src/components/NewMatch/NewMatchComponent';
import { User } from 'src/interfaces/user';

interface Props {
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
	user: User;
}

class NewMatch extends Component<Props> {

	render() {
		const { user } = this.props;
		return (
			<View style={{flexGrow: 1}}>
				<NewMatchComponent
					user={user}/>
			</View>
		);
	}
}

const mapStateToProps = (state: any) => ({
	user: state.user,
});

export const NewMatchPage = connect(
	mapStateToProps,
)(NewMatch);

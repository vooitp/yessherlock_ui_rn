// if(__DEV__) {
// 	// @ts-ignore
// 	import ('../ReactotronConfig');
// };
import React, { Component } from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { HomePage } from 'src/pages/Home';
import rootReducer from 'src/redux/reducers';
import { MatchesPage } from './pages/Matches';
import { NewMatchPage } from './pages/NewMatch';
const store = createStore(rootReducer, applyMiddleware(thunk));

const AppNavigator = createStackNavigator(
	{
		Home: HomePage,
		Matches: MatchesPage,
		NewMatch: NewMatchPage,
	},
	{
		initialRouteName: 'Home',
	},
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<AppContainer />
			</Provider>
		);
	}
}

import { User } from './user';

export class Card {
	id: string = '';
	title: string = '';
	question: string = '';
	answer: string = '';
	rating: number = 0;
	difficulty: number = 1;
	category: string = '';
	timesPlayed: number = 0;
	author: User | null = null;
	createdAt: string = new Date().toISOString();
}

export interface InitialFormValue {
	title: string;
	hostName: string;
	maxNumOfPlayers: number | string;
	minPlayerRank: number | string;
	onlyPrivate: boolean;
}

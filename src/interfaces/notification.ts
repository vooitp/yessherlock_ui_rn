export class Notification {
	id: string = '';
	text: string = '';
	shortBody: string = '';
	fullBody: string = '';
	createdAt: string = new Date().toISOString();
}

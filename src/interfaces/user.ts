export class User {
	id: string = '';
	facebookId: string = '';
	name: string = '';
	email: string = '';
	rank: number = 0;
	picture: string = '';
	createdAt: string = new Date().toISOString();
}

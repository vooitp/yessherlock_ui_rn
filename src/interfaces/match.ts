export interface MatchModel {
	id: string;
	type?: string;
	attributes: MatchAttributes;
}

export interface MatchAttributes {
	card?: any;
	maxNumOfPlayers?: string | number;
	createdAt?: string | Date;
	host?: any;
	isPrivate?: boolean;
	playerMinRank: number;
	players: any[];
	status?: string;
	title: string;
}

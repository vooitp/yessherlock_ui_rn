import { StyleSheet } from 'react-native';
import variables from '../../styles/variables';

export const styles = StyleSheet.create({
	containerFakeSidNav: {
		flex: 1,
		width: '20%',
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
	},
	outerContent: {
		flexGrow: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	mainBody: {
		flexDirection: 'row',
		flexGrow: 1,
		zIndex: 0,
	},
});

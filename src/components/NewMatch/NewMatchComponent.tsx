import React, { useEffect} from 'react';
import { View } from 'react-native';
import { User } from 'src/interfaces/user';
import HeaderComponent from '../Shared/Header/Header';
import { styles } from './NewMatchComponent.styles';
import { Form } from './Form/Form';
import { initializeForm, updateForm } from '../../redux/forms/actions';
import { connect } from 'react-redux';
import { postData } from '../../redux/matches/actions';

interface Props {
	user: User;
	form: any;
	initializeForm: () => void;
	inputChange: () => void;
	initialFormValue?: any;
	sendForm: any;
}

const formName = 'newMatch';

const initialFormValue = {
	title: '',
	maxNumOfPlayers: 0,
	minPlayerRank: 0,
	onlyPrivate: false,
	password: '123456',
	cardId: '5d9e48872a8ecfec9ff5254a',
};

const NewMatch = (props: Props) => {
	const {user, form, inputChange } = props;

	useEffect(() => {
		props.initializeForm();
	}, []);

	return (
		<View style={styles.outerContent}>
			<HeaderComponent user={ user } showNav={ false }/>
			<View style={styles.mainBody}>
				<View style={styles.containerFakeSidNav}/>
				<Form
					onButtonPress = { props.sendForm }
					form={ form }
					onChange={ inputChange }/>
			</View>
		</View>
	);
};

const mapStateToProps = (state: { form: any; }) => {
	return {
		form: state.form[formName] || {},
	};
};

const mapDispatchToProps = (dispatch, form) => {
	return {
		inputChange: (name: string, value: string | number | boolean) => {
			dispatch(updateForm(formName, { [name]: value} ));
		},
		initializeForm: () => {
			dispatch(initializeForm(formName, initialFormValue));
		},
		sendForm: postData(form),
	};
};

export const NewMatchComponent = connect(mapStateToProps, mapDispatchToProps)(NewMatch);
export { NewMatchComponent as NewMatch};

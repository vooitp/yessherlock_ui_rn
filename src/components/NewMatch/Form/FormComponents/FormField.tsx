import React from 'react';
import { View } from 'react-native';
import { InputSlider } from './InputSlider/InputSlider';
import { InputText } from './InputText/InputText';
import { InputCounter } from './Counter/InputCounter';
import { Checkbox } from './Checkbox/Checkbox';

interface Props {
	name: string;
	inputTitle?: string;
	filterType?: string;
	onChange?: () => void;
	value?: any;
	type?: string;
}

export const styles = {
	sidePadding: {
		paddingLeft: 40,
		paddingRight: 40,
	},
};

export const FormField = (props: Props) => {
	const renderInput = () => {
		switch (props.type) {
			case 'text':
				return <InputText
					name={props.name}
					value={props.value}
					inputChange={props.onChange}
					filterType={props.filterType}
					inputTitle={props.inputTitle}/>;
			case 'counter':
				return <InputCounter
					name={props.name}
					value={props.value}
					filterType={props.filterType}
					inputTitle={props.inputTitle}
					inputChange={props.onChange}/>;
			case 'checkbox':
				return <Checkbox
					name={props.name}
					value={props.value}
					filterType={props.filterType}
					inputTitle={props.inputTitle}
					inputChange={props.onChange}/>;
			case 'slider':
				return <View>
					<InputSlider
						value={props.value}
						name={props.name}
						inputChange={props.onChange}
						filterType={props.filterType}
						inputTitle={props.inputTitle}/>
				</View>;

			default:
				return null;
		}
	};

	return <View style={props.filterType !== 'filter' && styles.sidePadding}>
		{renderInput()}
	</View>;
};

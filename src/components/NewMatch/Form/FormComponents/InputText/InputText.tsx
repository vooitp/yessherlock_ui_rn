import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { styles } from '../FormField.styles';

interface Props {
	name: string;
	inputTitle: string | undefined;
	filterType?: string | undefined;
	value?: any;
	inputChange: any;
}

export const InputText = ( props: Props ) => {

	return <View>
		<Text style={props.filterType === 'filter' ?
			styles.inputFilterTitle : styles.inputTitle}>{props.inputTitle}</Text>

		<TextInput
			onChangeText={(value) => {
				props.inputChange(props.name, value);
			}}
			value={props.value}
			style={props.filterType === 'filter' ? styles.inputFilterText : styles.inputText}/>
	</View>;
};

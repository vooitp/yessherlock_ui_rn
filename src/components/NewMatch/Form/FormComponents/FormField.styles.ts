import { StyleSheet } from 'react-native';
import variables from 'src/styles/variables';
import { mixins } from 'src/styles/mixins';

export const styles = StyleSheet.create({
	filterRow: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 20,
	},
	flexDirRow: {
		flexDirection: 'row',
	},
	flexDirColumn: {
		flexDirection: 'column',
	},
	checkboxIcon: {
		resizeMode: 'contain',
		width: 32,
		height: 32,
		marginRight: 20,
	},
	checkboxTicIcon: {
		width: 24,
		height: 24,
		backgroundColor: variables.COLORS.BASIC_FONT,
		borderRadius: 3,
		position: 'absolute',
		top: 4,
		left: 4,
	},
	isVissible: {
		display: 'flex',
	},
	isHidden: {
		display: 'none',
	},
	inputTitle: {
		width: '80%',
		alignSelf: 'flex-start',
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.LARGE,
		color: variables.COLORS.BASIC_FONT,
		paddingBottom: 5,
		paddingTop: 5,
		textTransform: 'capitalize',
	},
	inputText: {
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.MEDIUM,
		color: variables.COLORS.THEME_ULTRA_LIGHT_BLUE,
		backgroundColor: 'transparent',
		borderWidth: 2,
		borderColor: variables.COLORS.BASIC_FONT,
		borderRadius: 30,
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 10,
		paddingRight: 10,
	},
	inputFilterTitle: {
		fontSize: variables.FONTS.SMALL,
		width: '80%',
		alignSelf: 'center',
		textAlign: 'center',
		...mixins.THEME_FONT,
		color: variables.COLORS.BASIC_FONT,
		paddingBottom: 5,
		paddingTop: 5,
		textTransform: 'capitalize',
	},
	inputFormTitle: {
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.LARGE,
		color: variables.COLORS.BASIC_FONT,
	},
	inputFilterText: {
		width: '80%',
		alignSelf: 'center',
		fontSize: variables.FONTS.SMALL,
		fontFamily: variables.FONTS.FONT_THEME,
		color: variables.COLORS.BASIC_FONT,
		backgroundColor: 'transparent',
		borderWidth: 2,
		borderColor: variables.COLORS.BASIC_FONT,
		borderRadius: 30,
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 10,
		paddingRight: 10,
	},
	// ------ COUNTER -------
	counterText: {
		fontSize: variables.FONTS.LARGE,
		fontFamily: variables.FONTS.FONT_THEME,
		color: variables.COLORS.BASIC_FONT,
	},
	counterWrapperFilter: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		alignSelf: 'center',
	},
	counterWrapper: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	arrowWrapper: {
		width: 18,
		height: 30,
		marginLeft: 10,
		marginRight: 10,

	},
	counterArrow: {
		resizeMode: 'contain',
		width: 18,
		height: 30,
	},
	hidden: {
		display: 'none',
	},
	vissible: {
		display: 'flex',
	},
});

import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { styles } from '../FormField.styles';

interface Props {
	name: string;
	inputTitle?: string;
	filterType?: string;
	inputChange: (name: string, p: any) => void;
	value?: boolean;
}

const ICONS = {
	rectIcon: require('src/images/rect_icon.png'),
};

export const Checkbox = (props: Props) => {

	const { filterType, inputTitle, value, inputChange, name } = props;

	const toggleBtn = () => {
		inputChange(name, !props.value);
	};

	return (
		<View style={filterType === 'filter' ? styles.filterRow : styles.inputFormRow}>
			<Text style={filterType === 'filter' ? styles.inputFilterTitle : styles.inputFormTitle}>
				{ inputTitle }
			</Text>
			<TouchableOpacity onPress={() => toggleBtn()}>
				<Image source={ICONS.rectIcon} style={styles.checkboxIcon}/>
				<View style={[styles.checkboxTicIcon, value ? styles.isVissible : styles.isHidden]}/>
			</TouchableOpacity>
		</View>
	);
};

import React from 'react';
import { View, Text } from 'react-native';
import Slider from '@react-native-community/slider';
import { styles } from '../FormField.styles';

interface Props {
	name: any;
	inputTitle: any;
	filterType: any;
	inputChange: any;
	value?: any | undefined;
}

const ICONS = {
	track: require('src/images/slider_track.png'),
	thumb: require('src/images/slider_thumb.png'),
};

const localStyles = {
	narrow: {
		width: 160, height: 26, alignSelf: 'center',
	},
	wide: {
		width: '100%', height: 26, alignSelf: 'center',
	},
};

export const InputSlider = (props: Props) => {
	return <View>
		<View>
			<Text style={props.filterType === 'filter' ?
				styles.inputFilterTitle : styles.inputTitle}>{props.inputTitle}: {props.value}</Text>
		</View>
		<Slider
			style={ props.filterType === 'filter' ? localStyles.narrow : localStyles.wide }
			minimumValue={0}
			maximumValue={10}
			step={1}
			onValueChange={(value) => {
				props.inputChange(props.name, value);
			}}
			minimumTrackTintColor='#FFFFFF'
			maximumTrackTintColor='#000000'
			thumbImage={ICONS.thumb}
			trackImage={ICONS.track}
		/>
	</View>;
};

import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { styles } from '../FormField.styles';

interface Props {
	name: string;
	inputTitle?: string;
	filterType?: string;
	inputChange: (name: string, p: any) => void;
	value?: any;
}

const ICONS = {
	arrowLeft: require('src/images/arrow_L.png'),
	arrowRight: require('src/images/arrow_R.png'),
};

export const InputCounter = (props: Props) => {

	const decrement = () => {
		props.inputChange(props.name, props.value - 1);
	};
	const increment = () => {
		props.inputChange(props.name, props.value + 1);
	};
	const handleVisibility = (side: string) => {
		return (props.value === 0 && side === 'left') ? styles.hidden : styles.vissible;
	};

	return <View style={styles.flexDirColumn}>
		<Text style={props.filterType === 'filter' ?
			styles.inputFilterTitle : styles.inputTitle}>{props.inputTitle}</Text>
		<View style={props.filterType === 'filter' ? styles.counterWrapperFilter : styles.counterWrapper}>
			<View style={styles.arrowWrapper}>
				<TouchableOpacity style={handleVisibility('left')} onPress={() => decrement()}>
					<Image source={ICONS.arrowLeft} style={styles.counterArrow}/>
				</TouchableOpacity>
			</View>
			<Text style={styles.counterText}>{props.value}</Text>
			<View style={styles.arrowWrapper}>
				<TouchableOpacity style={handleVisibility('right')} onPress={() => increment()}>
					<Image source={ICONS.arrowRight} style={styles.counterArrow}/>
				</TouchableOpacity>
			</View>
		</View>
	</View>;
};

import { StyleSheet } from 'react-native';
import variables from 'src/styles/variables';
import { mixins } from 'src/styles/mixins';

export const styles = StyleSheet.create({
	container: {
		flex: 5,
		backgroundColor: variables.COLORS.THEME_GREEN,
		justifyContent: 'space-between',
	},
	formWrapper: {
		paddingLeft: 40,
		paddingRight: 40,
	},
	listTitle: {
		backgroundColor: variables.COLORS.THEME_GREEN,
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.HUGE,
		textAlign: 'center',
		paddingBottom: 10,
		paddingTop: 10,
	},
	button__main: {
		backgroundColor: variables.COLORS.THEME_LIGHT_BLUE,
		width: '100%',
		alignSelf: 'flex-end',
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.HUGE,
		paddingTop: 10,
		paddingBottom: 10,
		textAlign: 'center',
	},
	invitationButton: {
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
		width: '80%%',
		alignSelf: 'center',
		paddingTop: 15,
		paddingBottom: 15,
		borderRadius: 8,
	},
	invitationButtonText: {
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.MEDIUM,
		textAlign: 'center',
	},
});

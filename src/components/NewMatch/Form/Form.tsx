import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { styles } from './Form.styles';
import { FormField } from './FormComponents/FormField';

interface Props {
	form: any;
	onChange: () => void;
	onButtonPress: (data) => void;
}

export class Form extends Component<Props> {
	render() {
		const {onChange, form, onButtonPress} = this.props;
		return (
			<View style={styles.container}>
				<Text style={styles.listTitle}>Create Match</Text>
				<FormField
					name='title'
					inputTitle='Name'
					onChange={onChange}
					value={form.title}
					type='text'/>
				<FormField
					name='maxNumOfPlayers'
					inputTitle='Number of participants'
					value={form.maxNumOfPlayers}
					onChange={onChange}
					type='counter'/>
				<TouchableOpacity onPress={() => []} style={styles.invitationButton}>
					<Text style={styles.invitationButtonText}>Send Invitation</Text>
				</TouchableOpacity>
				<FormField
					name='minPlayerRank'
					inputTitle='Minimal rating'
					value={form.minPlayerRank}
					onChange={onChange}
					type='slider'/>
				<FormField
					name='onlyPrivate'
					inputTitle='Private Match'
					onChange={onChange}
					value={form.onlyPrivate}
					type='checkbox'/>
				<FormField
					name='password'
					inputTitle='Password'
					value={form.password}
					onChange={onChange}
					type='text'/>
				<TouchableOpacity onPress={() => this.props.onButtonPress(form)}>
					<Text style={styles.button__main}>Create Match!</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

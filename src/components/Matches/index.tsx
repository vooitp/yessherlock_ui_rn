import React, { Component } from 'react';
import { View } from 'react-native';
import { User } from 'src/interfaces/user';
import { FlatListComponent } from '../Shared/FlatList/FlatList';
import HeaderComponent from '../Shared/Header/Header';
import { SideNav } from '../Shared/SideNav/SideNav';
import { styles } from './index.styles';

interface Props {
	user: User;
	matchesList: any[];
}

export class MatchesComponent extends Component<Props> {

	render() {
		const { user, matchesList } = this.props;
		return (
			<View style={styles.outerContent}>
				<HeaderComponent user={ user } showNav={ true }/>
				<View style={styles.mainBody}>
					<SideNav
						showIcons={ true }
						formName='matches'
						initialFormValue={{ maxNumOfPlayers: 6, minPlayerRank: 0, onlyPrivate: false }}/>
					<FlatListComponent matchesList={ matchesList }/>
				</View>
			</View>
		);
	}
}

import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	outerContent: {
		flexGrow: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	mainBody: {
		flexDirection: 'row',
		flexGrow: 1,
		zIndex: 0,
	},
});

import React, { PureComponent } from 'react';
import { ImageBackground, Platform, Text, View } from 'react-native';
import { LoginButton } from 'react-native-fbsdk';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { User } from 'src/interfaces/user';
import { FacebookService } from 'src/services/facebookService';
import { wait } from 'src/utils/wait';
import { styles } from './index.styles';

interface Props {
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
	user: User | null;
	loginUser(facebookId: string, name: string, email: string, picture: string): Promise<boolean>;
	fetchUserData(): Promise<User | any>;
}

// tslint:disable-next-line:no-var-requires
const bgImage = require('../../images/bg_View.jpg');

export class HomeComponent extends PureComponent<Props> {

	public subTitle = 'Ready to \n    challenge your \n        mind?';
	public footerTitle = 'Nothing is published without your approval';

	componentDidMount() {
		this.getUserData();
	}

	async getAccessToken() {
		const { data: accessToken } = await wait(FacebookService.getAccessToken());
		return accessToken;
	}

	async getUserData() {
		const accessToken = await this.getAccessToken();
		if (accessToken) {
			const { data: facebookUser } = await wait(
				FacebookService.getUserData(accessToken, ['email', 'name', 'picture.width(300).height(300)']),
			);

			if (facebookUser) {
				const { id: facebookId, name, email, picture: { data: { url: pictureUrl } } } = facebookUser;
				const { data: isLoggedIn } = await wait(this.props.loginUser(facebookId, name, email, pictureUrl));
				if (isLoggedIn) {
					const { data: user } = await wait(this.props.fetchUserData());
					if (user) {
						this.props.navigation.navigate('Matches');
					}
				}
			}
		}
	}

	onLoginFinished = (error: any, result: any) => {
		if (!error && !result.isCancelled) {
			this.getUserData();
		}
	}

	onLogoutFinished = () => {
		// remove user data
	}

	render() {
		const instructions = Platform.select({
			android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu',
			ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
		});
		return (
			<ImageBackground style={styles.imgBackground} source={bgImage}>
				<View style={styles.container}>
					<View style={styles.titleWrapper }>
						<Text style={styles.title}>{'Yes,'}</Text>
						<Text style={styles.title}>{'Sherlock!'}</Text>
					</View>
					<View>
						<Text style={styles.subTitle}>{this.subTitle}</Text>
					</View>
					<View style={styles.loginWrapper}>
						<LoginButton
							onLoginFinished={this.onLoginFinished}
							onLogoutFinished={this.onLogoutFinished}
							readPermissions={FacebookService.readPermissions}/>
					</View>
					<View>
						<Text style={styles.footerTitle}>{this.footerTitle}</Text>
					</View>
				</View>
			</ImageBackground>
		);
	}
}

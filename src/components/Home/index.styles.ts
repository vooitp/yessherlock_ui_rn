import { StyleSheet } from 'react-native';

const orange  = '#d5c04c';

export const styles = StyleSheet.create({
	container: {
		padding: 40,
		flex: 1,
	},
	title: {
		color: orange,
		fontSize: 50,
		fontWeight: 'bold',
		fontFamily: 'BerkshireSwash-Regular',
	},
	titleWrapper: {
		marginBottom: 40,
	},
	instructions: {
		color: orange,
		marginBottom: 5,
		textAlign: 'center',
	},
	subTitle: {
		color: orange,
		fontSize: 28,
		fontWeight: 'bold',
		fontFamily: 'BerkshireSwash-Regular',
		marginBottom: 40,
	},
	loginWrapper: {
		flex: 1,
		alignItems: 'center',
	},
	imgBackground: {
		width: '100%',
		height: '100%',
		flex: 1,
	},
	footerTitle: {
		fontWeight: 'bold',
		fontFamily: 'BerkshireSwash-Regular',
		fontSize: 16,
		color: orange,
	},
});

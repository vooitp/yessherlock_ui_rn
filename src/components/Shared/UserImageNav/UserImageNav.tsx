import React, { useState } from 'react';
import { Animated, Image, Text, TouchableOpacity, View } from 'react-native';
import { connect} from 'react-redux';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { toggle} from 'src/redux/userRibbon/actions';
import { useAnimation } from 'src/hooks/hooks';
import { styles} from './UserImageNav.styles';

// tslint:disable-next-line:no-var-requires
const loopImage = require('src/images/loop_xl.png');

interface Props {
	user: string | undefined;
	picture: string | undefined;
	data: string | undefined;
	url: string | undefined;
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface Props {
	toggleMenu: any;
	onToggleButton: any;
}

export const UserImageNavComponent = (props: Props ) => {

	const animationConfig = () => {
		return {
			opacity: animation.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 1],
			}),
			height: animation.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 160],
			}),
			overflow: 'hidden',
		};
	};

	const [doAnimation, setDoAnimation] = useState(false);
	const animation = useAnimation({ doAnimation, duration: 170 });
	const { user } = props;
	return (
		<View>
			<Image source={loopImage} style={styles.imageLoop}/>
			<View style={[styles.container, {overflow: 'hidden'}]}>
				<View style={styles.imageWrapper}>
					<Image source={{uri: user.picture}} style={styles.imageUser}/>
				</View>
				<Animated.View
					doAnimation={doAnimation}
					style={[styles.navItemList, animationConfig()]}>
					{/* tslint:disable-next-line:indent */}
					  {/*style={[styles.navItemList, toggleMenu ? styles.navHidden : '']}>*/}
					<Text style={styles.navItem}>List of matches</Text>
					<View style={styles.navItemSeparator}/>
					<Text style={styles.navItem}>Create Match</Text>
				</Animated.View>
				{/*<TouchableOpacity onPress={() => {onToggleButton}} style={styles.ribbonWrapper}>*/}
				<TouchableOpacity onPress={() => setDoAnimation(!doAnimation)} style={styles.ribbonWrapper}>
					<View style={styles.ribbon}/>
				</TouchableOpacity>
			</View>
		</View>
	);

};

const mapStateToProps = (state) => ({
	toggleMenu: state.value.val,
});

const mapDispatchToProps = (dispatch) => ({
	onToggleButton: () => {
		dispatch(toggle());
	},
});

const ConnectedUserNavComponent = connect(mapStateToProps, mapDispatchToProps)(UserImageNavComponent);
export { ConnectedUserNavComponent as UserImageNavComponent };

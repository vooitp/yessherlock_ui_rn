import { StyleSheet } from 'react-native';
import variables from '../../../styles/variables';
import { mixins } from '../../../styles/mixins';

export const styles = StyleSheet.create({
	container: {
		flex: 5,
		backgroundColor: variables.COLORS.THEME_GREEN,
		alignSelf: 'stretch',
		overflow: 'hidden',
	},
	listTitle: {
		backgroundColor: variables.COLORS.THEME_GREEN,
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.HUGE,
		textAlign: 'center',
		paddingBottom: 10,
		paddingTop: 10,
	},
	listItem: {
		backgroundColor: variables.COLORS.THEME_DARK_GREEN,
		marginBottom: 5,
		borderLeftColor: variables.COLORS.THEME_BORDER,
		borderLeftWidth: 4,
	},
	listItemTitle: {
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.LARGE,
		width: '100%',
		textAlign: 'center',
		marginBottom: 10,
		marginTop: 10,
	},
	additionalText: {
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.SMALL,
		textAlign: 'center',
	},
	timeInfoText: {
		alignItems: 'flex-end',
	},
	row: {
		flexDirection: 'row',
	},
	icon__floating: {
		position: 'absolute',
		right: 0,
		top: 0,
	},
});

import React, { Component } from 'react';
import {Text, View, Image, FlatList, Button} from 'react-native';
import { styles } from './FlatList.styles';
import { generalStyles } from '../../../styles/general';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

// tslint:disable-next-line:no-var-requires
const timerIcon = require('./../../../images/icon_clock.png');
// tslint:disable-next-line:no-var-requires
const lockIcon = require('./../../../images/icon_lock.png');

export class FlatListComponent extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { matchesList, navigation } = this.props;
		return (
			<View style={styles.container}>
				<Text style={styles.listTitle}>{'List of Matches'}</Text>
				{/*<Button onPress={() => navigation.navigate('NewMatch')} title='Create match' />*/}

				<FlatList data={ matchesList } renderItem={({ item }) => {
						return<View style={styles.listItem} key={item.id}>
						<Text style={styles.listItemTitle}>
							{item.title}
							{item.restricted && <Image source={lockIcon} style={[generalStyles.icon, styles.icon__floating]}/>}</Text>
						<Text style={styles.additionalText}>Host:
							<Text style={generalStyles.text__blue}>{item.host.name}</Text>
						</Text>
						<Text style={styles.additionalText}>Card:
							<Text style={generalStyles.text__blue}>{item.card.title}</Text></Text>
						<View style={styles.row}>
							<View style={generalStyles.col50}>
								<View>
									<Image source={timerIcon}/>
								</View>
							</View>
							<View style={generalStyles.col50}>
								<View>
									<Image source={timerIcon}/>
								</View>
							</View>
						</View>
					</View>; }}/>
			</View>
		);
	}
}

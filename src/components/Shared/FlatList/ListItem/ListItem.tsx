import React from 'react';
import { Text, View, Image } from 'react-native';
import { styles } from './ListItem.styles';
import { generalStyles } from 'src/styles/general';
// tslint:disable-next-line:no-var-requires
const timerIcon = require('src/images/icon_clock.png');
// tslint:disable-next-line:no-var-requires
const lockIcon = require('src/images/icon_lock.png');

export const ListItemComponent = (props) => {
	return <View style={styles.listItem} key={props.item.id}>
		{props.item.restricted ? <Image source={lockIcon} style={[generalStyles.icon, styles.icon__floating]}/> : null}
		<Text style={styles.listItemTitle}>{props.item.title} </Text>
		<Text style={styles.additionalText}>Host: <Text style={generalStyles.text__blue}>{props.item.host}</Text></Text>
		<Text style={styles.additionalText}>Card: <Text style={generalStyles.text__blue}>{props.item.card}</Text></Text>
		<View style={styles.row}>
			<View style={generalStyles.col50}>
				<View>
					<Image source={timerIcon}/>
				</View>
			</View>
			<View style={generalStyles.col50}>
				<View>
					<Image source={timerIcon}/>
				</View>
			</View>
		</View>
	</View>;
};

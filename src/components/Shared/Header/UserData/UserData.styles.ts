import { StyleSheet } from 'react-native';
import variables from '../../../../styles/variables';
import { mixins } from '../../../../styles/mixins';

export const localVariables = {
	CIRCLE_WIDTH: 120,
	CIRCLE_HEIGHT: 120,
	STATS_ICON_HEIGHT: 26,
	STATS_ICON_WIDTH: 26,
	MY_COLL_RIBBON_WIDTH: 45,
	MY_COLL_RIBBON_HEIGHT: 45,
};

export const styles = StyleSheet.create({
	rating: {
		fontSize: variables.FONTS.LARGE,
		color: variables.COLORS.BASIC_FONT,
		...mixins.THEME_FONT,
	},

	name: {
		fontSize: variables.FONTS.HUGE,
		color: variables.COLORS.BASIC_FONT,
		...mixins.THEME_FONT,
	},

	myCollection: {
		flexDirection: 'row',
		alignItems: 'center',
		alignSelf: 'flex-end',
		width: '0%',
		marginTop: 20,
		position: 'relative',
	},

	myCollectionTitle: {
		paddingTop: 10,
		paddingBottom: 10,
		width: '90%',
		marginLeft: localVariables.MY_COLL_RIBBON_WIDTH / 2,
		display: 'flex',
		textAlign: 'center',
		...mixins.THEME_FONT,
		fontSize: variables.FONTS.LARGE,
		color: variables.COLORS.BASIC_FONT,
		zIndex: 1,
	},

	myCollectionRibbon : {
		height: variables.MY_COLL_RIBBON_HEIGHT,
	},

	myCollectionRibbon_linearGrad: {
		width: localVariables.MY_COLL_RIBBON_WIDTH,
		height: localVariables.MY_COLL_RIBBON_HEIGHT,
		borderTopLeftRadius: localVariables.MY_COLL_RIBBON_WIDTH / 2,
		borderBottomLeftRadius: localVariables.MY_COLL_RIBBON_WIDTH / 2,
		backgroundColor: variables.COLORS.THEME_DARK_BLUE,
		zIndex: 0,
		position: 'absolute',
		left: -5,
	},

	linearGrad: {
		width: '90%',
		marginLeft: localVariables.MY_COLL_RIBBON_WIDTH / 2,
	},
});

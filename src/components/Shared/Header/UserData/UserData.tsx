import React, { Component } from 'react';
import {View, Text, Dimensions} from 'react-native';
import { styles } from './UserData.styles';
import LinearGradient from 'react-native-linear-gradient';
import { gradientColors } from 'src/styles/mixins';

interface Props {
	user: any;
	myCollectionTitle: any;
}

export class UserData extends Component {

	getDeviceWidth() {
		return Math.round(Dimensions.get('window').width);
	}

	getRibbonWidth() {
		return { width: this.getDeviceWidth() / 2};
	}

	render() {
		const { user, myCollectionTitle } = this.props;

		return (
			<View>
				<Text style={styles.name}>{user.name}</Text>
				<Text>
					<Text style={styles.rating}>Rating: </Text><Text style={styles.rating}>{user.rank}</Text>
				</Text>
				<View style={[styles.myCollection, this.getRibbonWidth()]}>
					<LinearGradient start={{x: 0, y: 0}}
									end={{x: 0.5, y: 0}}
									colors={[gradientColors.ribbonGradient.gradientColorFrom, gradientColors.ribbonGradient.gradientColorTo]}
									style={styles.myCollectionRibbon_linearGrad}>
						<View style={styles.myCollectionRibbon}/>
					</LinearGradient>
					<LinearGradient start={{x: 0, y: 0}}
									end={{x: 1, y: 0}}
									colors={[gradientColors.collectionTitle.gradientColorFrom, gradientColors.collectionTitle.gradientColorTo]}
									style={styles.linearGrad}>
						<Text style={styles.myCollectionTitle}>{myCollectionTitle}</Text>
					</LinearGradient>

				</View>
			</View>
		);
	}
}

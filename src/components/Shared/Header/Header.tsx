import React, { PureComponent } from 'react';
import { Image, View } from 'react-native';
import { UserImageNavComponent } from '../UserImageNav/UserImageNav';
import { styles } from './Header.styles';
import { UserData } from './UserData/UserData';
import { User } from 'src/interfaces/user';
// tslint:disable-next-line:no-var-requires
const statsIcon = require('../../../images/icon_stats_btn.png');

interface Props  {
	user: User;
	showNav: boolean;
}

export default class HeaderComponent extends PureComponent<Props> {

	public myCollectionTitle = 'My collection';

	render() {
		const { user, showNav } = this.props;
		return (
			<View style={styles.container}>
				<View style={styles.userImgWrapper}>
					{ showNav && <UserImageNavComponent user={ user }/> }
				</View>
				<View style={styles.userDataWrapper}>
					<UserData user={ user } myCollectionTitle={ this.myCollectionTitle }/>
				</View>
				<View style={styles.statsIconRibbon}>
					<View style={styles.iconWrapper}>
						<View style={styles.statsIconBtnBorder}/>
						<Image source={ statsIcon } style={styles.statsIconBtn}/>
					</View>
				</View>
			</View>
		);
	}
}

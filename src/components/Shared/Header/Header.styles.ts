import { StyleSheet } from 'react-native';
import variables from '../../../styles/variables';

export const localVariables = {
	CIRCLE_WIDTH: 120,
	CIRCLE_HEIGHT: 120,
	STATS_ICON_HEIGHT: 26,
	STATS_ICON_WIDTH: 26,
	MY_COLL_RIBBON_WIDTH: 45,
	MY_COLL_RIBBON_HEIGHT: 45,
};

export const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
		width: '100%',
		zIndex: 10,
	},
	userImgWrapper: {
		width: '33%',
	},
	userDataWrapper: {
		width: '66%',
		paddingTop: 12,
		paddingBottom: 24,
		paddingLeft: 12,
	},
	statsIconRibbon: {
		position: 'absolute',
		right: -50,
		top: -50,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: variables.COLORS.THEME_LIGHT_BLUE,
		width: localVariables.CIRCLE_WIDTH,
		height: localVariables.CIRCLE_HEIGHT,
		borderRadius: localVariables.CIRCLE_WIDTH / 2,
	},
	iconWrapper: {
		position: 'absolute',
		left: 24,
		top: 60,
	},
	statsIconBtnBorder: {
		width: localVariables.STATS_ICON_WIDTH + 8,
		height: localVariables.STATS_ICON_HEIGHT + 8,
		borderRadius: (localVariables.STATS_ICON_HEIGHT + 8) / 2,
		borderColor: variables.COLORS.THEME_ULTRA_LIGHT_BLUE,
		borderWidth: 6,
	},
	statsIconBtn: {
		top: 4,
		left: 4,
		position: 'absolute',
		width: localVariables.STATS_ICON_WIDTH,
		height: localVariables.STATS_ICON_HEIGHT,
	},

});

import { StyleSheet } from 'react-native';
import variables from '../../../styles/variables';

const localVariables = {
	ICON_CIRCLE_HEIGHT: 50,
	ICON_CIRCLE_WIDTH: 50,
	ICON_CIRCLE_MARGIN: 4,
};

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
	},
	container__centered: {
		justifyContent: 'center',
	},
	// main icon in filter
	navIcon: {
		alignSelf: 'flex-start',
		alignItems: 'center',
		justifyContent: 'center',
		width: localVariables.ICON_CIRCLE_WIDTH,
		height: localVariables.ICON_CIRCLE_HEIGHT,
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
		borderColor: variables.COLORS.BASIC_FONT,
		top: 5,
		borderRadius: 50 / 2,
		borderWidth: 1,
		marginTop: 0,
		marginBottom: 10, // same for confirmIcon
		marginLeft: 10, // same for confirmIcon
		zIndex: 100,
	},
	// orange icon background
	navIconBackground: {
		width: '100%',
		height: localVariables.ICON_CIRCLE_HEIGHT + 10,
		backgroundColor: 'transparent',
		borderColor: variables.COLORS.BASIC_FONT,
		borderBottomRightRadius: 50 / 2,
		borderTopRightRadius: 50 / 2,
		borderTopLeftRadius: 0,
		borderBottomLeftRadius: 0,
		zIndex: 10,
	},
	navIconBackground_Colored: {
		backgroundColor: variables.COLORS.BASIC_FONT,
	},
	navIconImage: {
		resizeMode: 'contain',
		width: 24,
		height: 24,
	},
	navIconTitle: {
		color: 'white',
		fontFamily: variables.FONTS.FONT_THEME,
		fontSize: 12,
		position: 'absolute',
		bottom: 5,
		right: 2,
	},
	confirmIcon: {
		backgroundColor: variables.COLORS.BASIC_FONT,
		width: localVariables.ICON_CIRCLE_WIDTH - localVariables.ICON_CIRCLE_MARGIN,
		height: localVariables.ICON_CIRCLE_HEIGHT - localVariables.ICON_CIRCLE_MARGIN,
		borderRadius: 50 / 2,
		zIndex: 110,
		position: 'absolute',
		left: 10 + localVariables.ICON_CIRCLE_MARGIN / 2,
		top: 5 + localVariables.ICON_CIRCLE_MARGIN / 2,
	},
	// tooltip
	filterWrapper: {
		width: '80%',
		alignSelf: 'flex-end',
		justifyContent: 'center',
		overflow: 'hidden',
	},
	filterTooltip: {
		backgroundColor: variables.COLORS.THEME_NAVY_BLUE,
		borderBottomRightRadius: localVariables.ICON_CIRCLE_WIDTH,
		borderTopRightRadius: localVariables.ICON_CIRCLE_WIDTH,
		borderRightColor: variables.COLORS.BASIC_FONT,
		borderTopColor: variables.COLORS.BASIC_FONT,
		borderBottomColor: variables.COLORS.BASIC_FONT,
		borderWidth: 1,
		position: 'absolute',
		top: '50%',
		width: '100%',
		left: 0,
		height: '100%',
		overflow: 'hidden',
		zIndex: 0,
		transform: [{
			translateY: -localVariables.ICON_CIRCLE_HEIGHT / 2 - 6,
		}],
	},
	filterTooltip__Active: {
		width: localVariables.ICON_CIRCLE_WIDTH + 240,
	},
	topLayer: {
		zIndex: 100,
	},
});

import React, { useEffect, useState } from 'react';
import { Animated, Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import { styles} from './SideNav.styles';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { FormField} from '../../NewMatch/Form/FormComponents/FormField';
import { useAnimation } from 'src/hooks/hooks';
import { connect } from 'react-redux';
import { initializeForm, updateForm } from 'src/redux/forms/actions';

// tslint:disable-next-line:no-var-requires
const ICONS = {
	lockIcon: require('src/images/icon_lock.png'),
	newStarIcon: require('src/images/icon_new.png'),
	keyIcon: require('src/images/icon_key.png'),
	userIcon: require('src/images/icon_user.png'),
	usersIcon: require('src/images/icon_users.png'),
	rect4Icon: require('src/images/icon_4_rect.png'),
	fileIcon: require('src/images/icon__file.png'),
	sherlockIcon: require('src/images/icon_sherlock.png'),
	sortIcon: require('src/images/icon_sort.png'),
	statsIcon: require('src/images/icon_stats.png'),
};

interface Props {
	form: any;
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
	showIcons: boolean;
	formName: string;
	initialFormValue?: any;
	initializeForm: () => void;
	inputChange: () => void;
}

interface FilterModel {
	name: any;
	nameType: string;
	titleText: string;
	iconText: string;
	icon: any;
	filterType: string;
}

const SideNav = (props: Props) => {
	useEffect(() => {
		props.initializeForm();
	}, []);

	const navOptions = [
		{
			name: 'title',
			nameType: 'text',
			titleText: 'Title',
			iconText: 'title',
			icon: ICONS.fileIcon,
		}, {
			name: 'hostName',
			nameType: 'text',
			titleText: 'Host name',
			iconText: 'host',
			icon: ICONS.userIcon,
		}, {
			name: 'maxNumOfPlayers',
			nameType: 'counter',
			titleText: 'Number of players',
			iconText: '',
			icon: ICONS.usersIcon,
		}, {
			name: 'onlyPrivate',
			nameType: 'checkbox',
			titleText: 'Show private matches',
			iconText: '',
			icon: ICONS.lockIcon,
		}, {
			name: 'minPlayerRank',
			nameType: 'slider',
			titleText: 'Min.Rating',
			iconText: '',
			icon: ICONS.statsIcon,
		},
	];

	const screenWidth = Math.round(Dimensions.get('window').width);
	const renderIcon = (icon) => {
		return <Image source={icon} style={styles.navIconImage}/>;
	};

	const [doAnimation, setAnimation] = useState(new Array(navOptions.length).fill(false));
	const animations = navOptions.map((item, i) => {
		return useAnimation({ doAnimation: doAnimation[i], duration: 170 });
	});

	const animationConfig = (i: number) => {
		return {
			opacity: animations[i].interpolate({
				inputRange: [0, 1],
				outputRange: [0, 1],
			}),
			width: animations[i].interpolate({
				inputRange: [0, 1],
				outputRange: [0, 0.65 * screenWidth],
			}),
			overflow: 'hidden',
		};
	};

	const setAnimationItem = (index: number) => {
		const copyAnimation = doAnimation.map(item => item);
		copyAnimation.fill(false);
		copyAnimation[index] = !copyAnimation[index];
		setAnimation(copyAnimation);
	};

	const fetchFilterMatches = (form) => {
		return true;
	};

	const navItemsList = navOptions.map((navItem: FilterModel, i: number) => {
		return (
			<View key={i}>
				<View style={[styles.navIconBackground, doAnimation[i] ? styles.navIconBackground_Colored : null]}>
					{ props.form[navItem.name] !== props.initialFormValue[navItem.name] ?
						<TouchableOpacity onPress={() => fetchFilterMatches(props.form)} style={styles.confirmIcon}/> : null }
					<TouchableOpacity
						onPress={() => setAnimationItem(i)}
						style={styles.navIcon}>
						{renderIcon(navItem.icon)}
						<Text style={styles.navIconTitle}>{navItem.iconText}</Text>
					</TouchableOpacity>
				</View>
				<Animated.View
					style={[styles.filterTooltip, animationConfig(i)]}
					doAnimation={doAnimation[i]}>
					<View style={styles.filterWrapper}>
						<FormField
							name={navItem.name}
							value={props.form[navItem.name]}
							inputTitle={navItem.titleText}
							onChange={props.inputChange}
							type={navItem.nameType}
							filterType='filter'/>
					</View>
				</Animated.View>
			</View>
		);
	});
	return (
		<View style={[styles.container, styles.container__centered, styles.topLayer]}>
			<View>
				{ props.showIcons ? navItemsList : null }
			</View>
		</View>
	);
};

const mapStateToProps = (state: { form: any; }, props: Props) => {
	// console.log(state.form)
	return {
		form: state.form[props.formName] || {},
	};
};

const mapDispatchToProps = (dispatch, props: Props) => {
	return {
		inputChange: (name: string, value: string | number) => {
			dispatch(updateForm(props.formName, {[name]: value}));
		},
		initializeForm: () => {
			dispatch(initializeForm(props.formName, props.initialFormValue));
		},
	};
};
const SideNavComponent = connect(mapStateToProps, mapDispatchToProps)(SideNav);
export { SideNavComponent as SideNav };

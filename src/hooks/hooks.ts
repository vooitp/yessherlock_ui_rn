import { Animated, Easing } from 'react-native';
import { useState, useEffect } from 'react';

export const useAnimation = ({ doAnimation, duration }) => {
	const [state] = useState(new Animated.Value(0));

	useEffect(() => {
		Animated.timing(state, {
			toValue: doAnimation ? 1 : 0,
			duration,
			easing: Easing.linear,
		}).start();
	}, [doAnimation]);

	return state;
};
